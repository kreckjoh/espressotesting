package com.example.i343658.espressotesting

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_view.view.*

/**
 * Created by Johana Emma Křečková on 12/8/18.
 */
class ColorAdapter(private val personDataset: List<String>, val clickListener: (String) -> Unit) :
    RecyclerView.Adapter<ColorAdapter.MyViewHolder>() {
    class MyViewHolder(val rowView: View) : RecyclerView.ViewHolder(rowView)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ColorAdapter.MyViewHolder {
        val rowView = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_view, parent, false) as View
        return MyViewHolder(rowView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: String = personDataset.get(position)
        holder.rowView.color.text = item
        holder.rowView.setOnClickListener { clickListener(item) }
    }

    override fun getItemCount() = personDataset.size
}