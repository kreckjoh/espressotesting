package com.example.i343658.espressotesting

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.VISIBLE
import android.view.View.GONE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import android.text.TextUtils

val SAVED_COLOR = "COLOR"

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        age_edit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (age_edit.text.isEmpty()) {
                    age_alert.visibility = VISIBLE
                } else {
                    age_alert.visibility = GONE
                }
            }
        })

        email_edit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                if (isValidEmail(email_edit.text)) {
                    email_alert.visibility = GONE
                } else {
                    email_alert.visibility = VISIBLE
                }
            }
        })

        save.setOnClickListener {
            if (!isValidEmail(email_edit.text) || age_edit.text.isEmpty()) {
                showDialog()
            } else {
                age_edit.text.clear()
                email_edit.text.clear()
                pet_color_view.visibility = GONE
            }
        }

        viewManager = LinearLayoutManager(this)
        val colors = LinkedList<String>()
        colors.add(getString(R.string.green))
        colors.add(getString(R.string.red))
        colors.add(getString(R.string.blue))
        colors.add(getString(R.string.yellow))
        viewAdapter = ColorAdapter(colors) {
            setColorView(it)
        }
        recyclerView = findViewById<RecyclerView>(R.id.color_list).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        val footer = findViewById<TextView>(R.id.pet_color_view)
        footer.setBackgroundColor(Color.LTGRAY)
        footer.visibility = View.GONE

        val items = arrayOf(
            getString(R.string.cat), getString(R.string.dog), getString(R.string.hamster), getString(R.string.other))
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items)
        pet_list.adapter = adapter
        pet_list.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            footer.text = items[position]
            footer.visibility = View.VISIBLE
        }
    }

    private fun setColorView(it: String) {
        pet_color_view.visibility = VISIBLE
        when (it) {
            getString(R.string.green) -> pet_color_view.setBackgroundColor(Color.GREEN)
            getString(R.string.red) -> pet_color_view.setBackgroundColor(Color.RED)
            getString(R.string.blue) -> pet_color_view.setBackgroundColor(Color.BLUE)
            getString(R.string.yellow) -> pet_color_view.setBackgroundColor(Color.YELLOW)
        }
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setMessage(getString(R.string.fill_fields))
        builder.setPositiveButton(getString(R.string.ok)) { dialog, which ->
            dialog.cancel()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(SAVED_COLOR, (pet_color_view.background as ColorDrawable).color)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        savedInstanceState?.getInt(SAVED_COLOR)?.let {
            pet_color_view.visibility = VISIBLE
            pet_color_view.setBackgroundColor(it)
        }
    }
}
